import express from 'express';
import logger from 'winston-color';
import mongoose from 'mongoose';
import dotenv from 'dotenv';
dotenv.config();

const app = express();
app.listen(process.env.PORT, () => logger.info(`Application is running on port ${process.env.PORT}`));

// Database connection
mongoose.connect(process.env.DB_URL);
// On database connection
mongoose.connection.on('connected', () => {
  logger.info('Connected to database:', process.env.DB_URL);
});
// on databse error
mongoose.connection.on('error', (err) => {
  logger.error('Database error:', err);
});

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.get('/', (req, res) => res.send('Hello World!'))

export default app;
